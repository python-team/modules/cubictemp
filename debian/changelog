cubictemp (2.0-2) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Fix watch file.
  * Remove python-dev from Build-Depends-Indep.
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove trailing whitespaces
  * Remove debian/pycompat, it's not used by any modern Python helper
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 16:59:07 -0500

cubictemp (2.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
     + bump Standards-Version to 3.8.1.
     + bump debhelper compatibility to 7.
     + add ${misc:Depends} to Depends.
  * Remove unneeded patches and patching system accordingly.
  * Update debian/copyright: licence is now MIT.
  * Update packaging to prevent FTBFS. Thanks to Josselin
    Mouette. (Closes: #516162)

 -- Mohammed Adnène Trojette <adn+deb@diwi.org>  Sat, 21 Mar 2009 19:56:53 +0100

cubictemp (0.4-4) UNRELEASED; urgency=low

  [ Piotr Ożarowski ]
  * Added Vcs-Svn and Vcs-Browser fields

  [ Sandro Tosi ]
  * debian/control
    - fix Vcs-Browser field
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Jan Dittberner ]
  * add debian/watch
  * add python to Build-Depends in debian/control because the clean rule
    needs it
  * use new python-support conventions
     + create debian/pyversions
     + remove XS-Python-Version and XB-Python-Version
  * bump Standards-Version to 3.8.0

  [ Carlos Galisteo ]
  * debian/control
    - Added Homepage field.

 -- Jan Dittberner <jan@dittberner.info>  Tue, 24 Jun 2008 21:42:31 +0200

cubictemp (0.4-3) unstable; urgency=low

  * Forgot to bump debian/compat.
  * Cubictemp is not GPL, so see below, not above. Thanks Christophe Mutricy

 -- Mohammed Adnène Trojette <adn+deb@diwi.org>  Wed, 28 Jun 2006 10:57:54 +0200

cubictemp (0.4-2) unstable; urgency=low

  * Use new Python layout (Closes: #373312)
     + add XS-Python-Version
     + add XB-Python-Version
     + bump dependency on python-support
     + bump build-dependency on cdbs
  * Switch from dpatch to quilt for patch management.
     + add Build-Depends on quilt and patchutils

 -- Mohammed Adnène Trojette <adn+deb@diwi.org>  Tue, 13 Jun 2006 22:00:11 +0200

cubictemp (0.4-1) unstable; urgency=low

  * Initial release (Closes: #350432)

 -- Mohammed Adnène Trojette <adn+deb@diwi.org>  Fri, 14 Apr 2006 11:30:45 +0100
